package ru.t1.stepanischev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.stepanischev.tm.model.TaskDTO;

import java.util.Collection;

public interface TaskRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks/";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping(value = "/findAll")
    Collection<TaskDTO> findAll();

    @GetMapping(value = "/findById/{id}")
    TaskDTO findById(@PathVariable("id") String id);

    @PostMapping(value = "/delete")
    void delete(@RequestBody TaskDTO task);

    @PostMapping(value = "/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping(value = "/save")
    void save(@RequestBody TaskDTO task);

}