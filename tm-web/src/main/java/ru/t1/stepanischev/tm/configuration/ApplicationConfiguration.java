package ru.t1.stepanischev.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.stepanischev.tm")
public class ApplicationConfiguration {
}
