package ru.t1.stepanischev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import java.util.Collection;

public interface IProjectRestEndpoint {

    void delete(ProjectDTO project);

    void deleteById(String id);

    @Nullable
    Collection<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findById(String id);

    @NotNull
    ProjectDTO save(ProjectDTO project);

}
