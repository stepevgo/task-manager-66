package ru.t1.stepanishchev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findByUserId(@NotNull final String userId);

    @Nullable
    Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    @Query("SELECT p FROM Task p WHERE p.user = :userId ORDER BY :sortType")
    List<Task> findAllByUserIdAndSort(@NotNull @Param("userId") String user, @NotNull @Param("sortType") String sortType);

    void deleteByProjectId(@NotNull final String projectId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

}