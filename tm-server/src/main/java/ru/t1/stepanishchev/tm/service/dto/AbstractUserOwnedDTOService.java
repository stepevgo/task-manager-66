package ru.t1.stepanishchev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.stepanishchev.tm.repository.dto.AbstractUserOwnedDTORepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M>
        implements IUserOwnedDTOService<M> {

    @Nullable
    protected AbstractUserOwnedDTORepository<M> repository;

}
